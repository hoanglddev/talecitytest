/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View} from 'react-native';

import Home from './ui/Home.js';
import ItineraryDetail from './ui/ItineraryDetail.js'
import { createStackNavigator, createAppContainer} from "react-navigation";

const RootStack = createStackNavigator(
  {
    home:
    {
      screen: Home,
      navigationOptions: () =>
      ({
        title: 'Home',
      }),
    },
    detail:
    {
      screen: ItineraryDetail,
      navigationOptions: () => ({
        title: 'Itinerary Detail',
      }),

    }
  },
  {
    initialRouteName: 'home'
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component<Props> {

  render() {
    return (
      <AppContainer />
    );
  }
}
