import React, {Component} from 'react';
import {StyleSheet,View, Text, FlatList} from 'react-native';

export default class ActivityDetail extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
        showsVerticalScrollIndicator = {false}
        data = {this.props.activities}
        renderItem = {({item}) =>
        <View>
          <Text style = {styles.txt_content_normal}>{item.startTime}</Text>

          <Text style = {styles.txt_content_normal}>{item.title}</Text>

          <Text style = {styles.txt_content_normal}>{item.description}</Text>

          <View style={styles.line_container}>
            <View style={styles.line}/>
          </View>
        </View>
        }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:10,
    backgroundColor: '#F5FCFF',
  },

  txt_content_normal: {
    color: 'black',
    fontSize: 18,
    marginTop: 10,
  },
  line_container: {
    width: '100%',
    marginTop: 10,
    height:1,
    paddingLeft:10,
    paddingRight:10,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'black',
  },
});
