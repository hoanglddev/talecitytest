import React, {Component} from 'react';
import {StyleSheet, View, ImageBackground, Text, FlatList, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import ActivityDetail from './ActivityDetail.js';
import data from './data.json';

export default class ItineraryDetail extends Component<Props> {

  constructor(props) {
    super(props)
    itineraryData = this.props.navigation.getParam('data', data[0])
    console.log("ItineraryDetail");
    console.log(itineraryData);
    this.state = {
      dataCustom: itineraryData.agendaDays,
      activities: itineraryData.agendaDays[0].activities,
      tabSelected: 0
    }
  }

  tabItemClick(item) {
    this.setState({
      activities: item.activities,
      tabSelected: item.id
    });
  }

  getTabStyle(item) {
    if(item.id == this.state.tabSelected) {
      return styles.tab_item_container_selected
    } else {
      return styles.tab_item_container
    }
  }

  getTabTitleStyle(item) {
    if(item.id == this.state.tabSelected)
      return styles.tab_item_title_selected
    else
      return styles.tab_item_title
  }

  render() {
    return(
        <View style = {styles.container}>
          <ImageBackground
            style={styles.img_cover}
            source={require('../img/itinerary_1.jpg')}>
              <Text style={styles.txt_title}>{itineraryData.title}</Text>
          </ImageBackground>

          <Text style={styles.txt_content_normal}>
            {itineraryData.description}
          </Text>

          <View style={styles.line_container}>
            <View style={styles.line}/>
          </View>

          <Text style={styles.txt_content_normal}>
            Hi {itineraryData.guestName}, this is your itinerary.
          </Text>

          <View style={styles.tab_container}>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              extraData={this.state}
              data={this.state.dataCustom}
              keyExtractor={(item, index) => index.toString()}
              renderItem= {({item, index}) =>
                <TouchableOpacity
                  style = {this.getTabStyle(item)}
                  onPress={this.tabItemClick.bind(this, item)}>
                  <Text style={this.getTabTitleStyle(item)}> {item.date} </Text>
                </TouchableOpacity>
              }
              />
          </View>

          <ActivityDetail activities={this.state.activities} />

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF'
  },
  img_cover: {
    width: "100%",
    height: 200,
    justifyContent: 'flex-end'
  },
  txt_title: {
    color: 'white',
    fontSize: 18,
    margin: 10,
    fontWeight: 'bold',
  },
  txt_content_normal: {
    color: 'black',
    fontSize: 18,
    margin: 10,
  },
  line_container: {
    width: '100%',
    height:1,
    paddingLeft:20,
    paddingRight:20,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'black',
  },
  tab_container: {
    width: '100%',
    backgroundColor: 'white',
    marginLeft: 10,
    marginRight: 10,
  },
  tab_item_container: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 4,
    paddingBottom: 4,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginRight: 10
  },
  tab_item_container_selected: {
    backgroundColor: 'blue',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 4,
    paddingBottom: 4,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginRight: 10
  },
  tab_item_title: {
    color: 'black',
    fontSize: 14
  },
  tab_item_title_selected: {
    color: 'white',
    fontSize: 14
  },
});
