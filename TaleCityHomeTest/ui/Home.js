import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput, TouchableOpacity, Alert} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import data from './data.json';

export default class Home extends Component<Props> {

  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      code: 0
    }
  }

  onActionGo = () => {
    var itinerary = data.find((item) => {
       return item.code == this.state.code;
      })

    if (itinerary == null) {
      Alert.alert(
        'M-Travel',
        this.state.code + " is wrong",
        [
          {
            text: 'Cancel',
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.navigate('detail', {data: itinerary})
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>M-Travel</Text>

        <Text style={styles.txt_enter_code}>Please enter iternarary code</Text>

        <TextInput
            style= {styles.ti_code}
            placeholder="Your Code"
            onChangeText = {(code) => this.setState({code})}
            value = {this.state.code}/>

        <TouchableOpacity style={styles.btn_go} onPress={this.onActionGo}>
          <Text style={styles.txt_go}>Go</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: '30%',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  txt_enter_code: {
    textAlign: 'center',
    color: 'black',
    marginTop: '30%',
    fontSize: 20,
  },
  ti_code: {
    width: "80%",
    height: 48,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    borderColor: "black",
    borderWidth: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 10,
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
  },
  btn_go: {
    width: "80%",
    height: 48,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    borderWidth: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: "blue",
    alignItems: 'center',
    justifyContent: 'center'
  },
  txt_go: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
});
